/*
------------------------------
--- INITIALISATION DU SITE ---
------------------------------
*/
//Configuration des libs et templates
require.config({
	paths: {
		//Chargement du loader de backbone
		loader : 'libs/backbone/loader',
		//Chargement de jQuery
		jQuery : 'libs/jquery/jquery',
		//Underscore
		Underscore : 'libs/underscore/underscore',
		//Chargement de backbone
		Backbone : 'libs/backbone/backbone',
		//Dossier contenant les templates
		templates: '../templates'
	}
});


require([
	// Chargement du script applicatif qui sera passé en paramètre
	'app'
], function(App){ //Le script applicatif est passé en paramètre
	//Appel de l'initialisasation de l'application
	App.initialize();
});

/*--- GESTION DE GOOGLE ANALYTICS (changer UA-XXXXX-X avec l'id du site) ---*/
/*var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
s.parentNode.insertBefore(g,s)}(document,'script'));*/