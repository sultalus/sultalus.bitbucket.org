define([
	//Chargement des librairies utilisées pour la vue
	'jQuery',
	'Underscore',
	'Backbone'
], function($, _, Backbone){ //Les librairies sont accessibles dans les paramètres de la vue par leur alias.
	//Vue pour les galeries d'images
	var slideShowView = Backbone.View.extend({
		el : $('.slide_show'),
		//gestion des évènements
		events : {
			'click .next':'showNextImage',
			'click .back':'showPreviousImage',
			'click .imageLink':'showImage',
			'keydown .list_mask':'keyboardSelection',
			'click .imageLink':'showImageLink'
		},
		initialize : function () {
			//alias de this
			var _this = this;
			
			//drapeau de détection
			var isSelected = false;
			
			//Pour chaque lien image
			this.el.find('.image_list_wrapper a')
			.each(function (e) {
				var $this = $(this);
				//si la classe selected est détectée
				if ($this.hasClass('selected')) {
					//on attribue l'élément au current index
					_this.currentIndex = e;
					//on met à jour le drapeau
					isSelected = true;
				}
				//Retire le focus du lien pour garder celui de frise
				$this.attr('tabindex', '-1');
				$this.attr('data-count', e);
				
				//on récupère l'index maximum
				_this.maxIndex = e;
			});
			
			//Si aucun élément n'est sélectionné
			if (!isSelected) {
				//on sélectionne le premier par défaut
				$(this.el.find('.image_list_wrapper a')[0]).addClass('selected');
			}
			
			//on appel le rendu
			this.render();
		},
		//méthode du rendu
		render : function () {
			
		},
		//index de l'image actuelle
		currentIndex : 0,
		//index maximum
		maxIndex : 0,
		//Récupération de l'image suivant l'index
		getIndexedImage : function (index) {
			return $(this.el.find('.image_list_wrapper a')[index]);
		},
		//met à jour le compteur d'image
		updateCounter : function (e) {
			this.el.find('.counter').html(""+(this.currentIndex+1)+"/"+(this.maxIndex+1));
		},
		getBatchImage : function ($image) {
			var $imageBatch = null;
			
			$image.parents().map(function (e) {
				//jquery de this
				var $this = $(this);
				if ($this.hasClass("batch_img")) {
					$imageBatch = $this;
				}
			});
			
			return $imageBatch;
		},
		//afficher l'image suivante
		showNextImage : function (e) {
			//Si l'index maximum est atteint
			if (this.currentIndex >= this.maxIndex) {
				//on retourne la méthode
				return;
			}
			
			this.el.find('.back').css({
				visibility:"visible"
			});
			
			//Récupération de l'image actuelle
			var $currentImage = this.getIndexedImage(this.currentIndex);
			//On enlève la sélection de l'image
			$currentImage.removeClass("selected");
			//on incrémente l'index de l'image
			this.currentIndex++;
			//Récupération de l'image suivante
			var $nextImage = this.getIndexedImage(this.currentIndex);
			//on met à jour le compteur
			this.updateCounter();
			//On ajoute la classe sélectionnée
			$nextImage.addClass('selected');
			//sélection du lot d'image parent
			var $imageBatch = this.getBatchImage($nextImage);
			//Sélection de l'englobant de la liste
			var $listWrapper = this.el.find(".image_list_wrapper");
			//Mise à jour de sa position
			$listWrapper.animate({
				left : -parseInt($imageBatch.css("width"))*$imageBatch.index()+"px"
			}, 100);
			//affichage de la nouvelle image
			this.showImage($nextImage.attr('href'));
			
			//Si l'index maximum est atteint
			if (this.currentIndex >= this.maxIndex) {
				this.el.find('.next').css({
					visibility:"hidden"
				});
			}
		},
		//afficher l'image suivante
		showPreviousImage : function (e) {
			//Si l'index minimum est atteint
			if (this.currentIndex <= 0) {
				//on retourne la méthode
				return;
			}
			
			this.el.find('.next').css({
				visibility:"visible"
			});
			
			//Récupération de l'image actuelle
			var $currentImage = this.getIndexedImage(this.currentIndex);
			//On enlève la sélection de l'image
			$currentImage.removeClass("selected");
			//on incrémente l'index de l'image
			this.currentIndex--;
			//Récupération de l'image suivante
			var $nextImage = this.getIndexedImage(this.currentIndex);
			//on met à jour le compteur
			this.updateCounter();
			//On ajoute la classe sélectionnée
			$nextImage.addClass('selected');
			//sélection du lot d'image parent
			var $imageBatch = this.getBatchImage($nextImage);
			//Sélection de l'englobant de la liste
			var $listWrapper = this.el.find(".image_list_wrapper");
			//Mise à jour de sa position
			$listWrapper.animate({
				left : -parseInt($imageBatch.css("width"))*$imageBatch.index()+"px"
			}, 100);
			//affichage de la nouvelle image
			this.showImage($nextImage.attr('href'));
			//Si l'index minimum est atteint
			if (this.currentIndex <= 0) {
				//on retourne la méthode
				this.el.find('.back').css({
					visibility:"hidden"
				});
			}
		},
		//navigation au clavier
		keyboardSelection : function (e) {
			switch (e.keyCode) {
				//fleche gauche
				case 37 :
					this.showPreviousImage();
				break;
				
				//fleche droite
				case 39 :
					this.showNextImage();
				break;
				
				//Par défaut : rien!
				default : break;
			}
		},
		//Afficher l'image
		showImage : function (urlImage) {
			//alias de this
			var _this = this;
			//Appel de la page
			$.ajax({
				//suivant l'url du lien'
				url : urlImage,
				//forçage du html
				dataType : "html",
				//quand le retour est ok
				success : function (data) {
					//on remplace le contenu de display image
					_this.el.find(".display_image").html($(data).find(".display_image").html());
					//on réinitialise prettyPhoto
					$('.pp_pic_holder').remove();
					$('.pp_overlay').remove();
					$('.ppt').remove();
					$('.prettyModal').prettyPhoto({autoplay:false,show_title:true,overlay_gallery:false,deeplinking:false,social_tools:''});
				}
			});
		},
		//afficher l'image
		showImageLink : function (e) {
			//gestion du lien pour IE
			var $link = (e.currentTarget) ? $(e.currentTarget) : $(e.srcElement);
			//on annule le lien
			e.preventDefault();
			//on récupère l'index
			this.currentIndex = parseInt($link.attr('data-count'));
			//On affiche l'image grace au lien
			this.showImage($link.attr('href'));
			//on met à jour le compteur
			this.updateCounter();
			//on supprime la classe CSS selected
			this.el.find(".selected").removeClass('selected');
			$link.addClass("selected");
			//Si l'index maximum est atteint - Si l'index minimum est atteint
			if (this.currentIndex >= this.maxIndex) {
				this.el.find('.next').css({
					visibility:"hidden"
				});
			}
			else {
				//on retourne la méthode
				this.el.find('.next').css({
					visibility:"visible"
				});
			}
			if (this.currentIndex <= 0) {
				//on retourne la méthode
				this.el.find('.back').css({
					visibility:"hidden"
				});
			}
			else {
				//on retourne la méthode
				this.el.find('.back').css({
					visibility:"visible"
				});
			}
		}
	});
	
	
	
	//vue pour la gestion des news en home-page
	var newsHomeView = Backbone.View.extend({
		el : $(".carousel_news"),
		filter : null,
		//Durée du focus
		focusTime : 0,
		//Rerise du focus après intervention humaine
		focusReplay : false,
		//liste des liens
		linkList : null,
		//TimeOut du focus
		focusTimeOut : null,
		//Gestion de l'index courrant
		currentIndex : 0,
		//Max index
		maxIndex : 0,
		//Gestion des évènements
		events : {
			"mouseenter a":"manualFocus",
			"mouseleave":"manualFocusOut"
		},
		//initialisation de la vue
		initialize : function (e) {
			//création du filtre
			this.filter = '<div class="filter"></div>';
			//récupération des datas de configuration
			//Durée du focus
			this.focusTime = this.el.attr('data-focus-time');
			//Replay du focus après intervention humaine
			this.focusReplay = (parseInt(this.el.attr('data-focus-replay')) === 1) ? true:false;
			//génération de la liste des liens
			this.linkList = this.el.find('a');
			//ajout du filtre pour tous les liens
			this.linkList.append('<div class="filter">Filtre</div>');
			//Indication de l'index max
			this.maxIndex = this.linkList.length - 1;
			//appel du rendu
			this.render();
			
			//Appel de la circulation du focus
			this.runFocus(this.currentIndex);
		},
		//rendu de la vue
		render : function (e) {
			
		},
		//Déclanche le focus automatique
		runFocus : function (index) {
			//Alias de this
			var _this = this;
			//on arrete l'animation principale
			window.clearTimeout(this.focusTimeOut);
			//Assignation de l'index
			this.currentIndex = index;
			//Affichage de tous les filtres
			this.linkList.find('.filter').show();
			//Masquage du focus courrant
			$(this.linkList[this.currentIndex]).find('.filter').hide();
			//Set Timeout du focus
			this.focusTimeOut = window.setTimeout(function (e) {
				//Si le focus n'est pas arrivé au bout on passe au suivant, sinon on revient au premier
				(_this.currentIndex+1 <= _this.maxIndex) ? _this.runFocus(_this.currentIndex+1):_this.runFocus(0);
			}, this.focusTime);
		},
		//prise de focus manuel
		manualFocus : function (e) {
			//On arrete le défilement
			window.clearTimeout(this.focusTimeOut);
			//on sélectionne le lien
			//gestion du lien pour IE
			var $link = (e.currentTarget) ? $(e.currentTarget) : $(e.srcElement);
			//Affichage de tous les filtres
			this.linkList.find('.filter').show();
			//Masquage le filtre courrant
			$link.find('.filter').hide();
		},
		//A la sortie du focs sur les news
		manualFocusOut : function (e) {
			//alias de this
			var _this = this;
			//Si le focus replay est désactivé
			if (!this.focusReplay) {
				return false;
			}
			//Pour chaque lien
			this.linkList.each(function (e) {
				//jquery de this
				var $this = $(this);
				//si le filtre est masqué
				if ($this.find(".filter").is(":hidden")) {
					//on reprend l'index depuis ici
					_this.currentIndex = e;
				}
			});
			//On reprend la course
			this.runFocus(_this.currentIndex);
		}
	});
	
	
	
	//Vue pour les galeries d'images avec pagination à points
	var carrouselDotView = Backbone.View.extend({
		el : $('.carrousel-dots'),
		listWrapper : null,
		list_dots : null,
		_width : 0,
		focusTime : 0,
		focusReplay : false,
		focusTimeOut : null,
		currentIndex : 0,
		maxIndex : 0,
		duration : 500,
		//gestion des évènements
		events : {
			'click .page-dot':'showImage',
			'focus .page-dot':'showImage'
		},
		initialize : function () {
			//alias de this
			var _this = this;
			//Durée du focus
			this.focusTime = this.el.attr('data-focus-time');
			//Replay du focus après intervention humaine
			this.focusReplay = (parseInt(this.el.attr('data-focus-replay')) === 1) ? true:false;
			//drapeau de détection
			var isSelected = false;
			//on défini le wrapper
			this.listWrapper = this.el.find(".image_dot_wrapper");
			//on défini la largeur de la boîte
			this._width = this.listWrapper.find('li:first-child').css('width');
			//on récupère le container des boutons
			this.list_dots = this.el.find('.list_dots');
			
			//Pour chaque puce d'image
			this.listWrapper.find('li').each(function (e){
				var $this = $(this);
				//classe CSS du 1er élément
				var _class = 0 == e ? ' selected' : '';
				//on crée un bouton contenant un lien
				var $button = $('<button class="page-dot' + _class + '" data-index="' + e + '"><a href="#" title="">' + e + '</a></button>');
				_this.list_dots.append($button);
				//Retire le focus du lien pour garder celui de frise
				$this.attr('tabindex', '-1');
				//Retire le focus du lien s'il existe
				$this.find('a').attr('tabindex', '-1');
				//Assignation du maxIndex
				_this.maxIndex = e;
			});
			
			//Assignation du currentIndex
			this.currentIndex = 0;
			
			//on appel le rendu
			this.render();
			//Appel de la circulation du focus
			this.runFocus(this.currentIndex);
		},
		//méthode du rendu
		render : function () {
		},
		//Déclanche le focus automatique
		runFocus : function(index) {
			//Alias de this
			var _this = this;
			//on arrete l'animation principale
			window.clearTimeout(this.focusTimeOut);
			//Assignation de l'index
			this.currentIndex = index;
			//Animation du carrousel
			this.showManualImage(this.currentIndex);
			//Set Timeout du focus
			this.focusTimeOut = window.setTimeout(function (e) {
				//Si le focus n'est pas arrivé au bout on passe au suivant, sinon on revient au premier
				(_this.currentIndex+1 <= _this.maxIndex) ? _this.runFocus(_this.currentIndex+1):_this.runFocus(0);
			}, this.focusTime);
		},
		//affiche l'image choisie
		showManualImage : function(index) {
			//alias de this
			var $target = this.list_dots.find('button:eq(' + parseInt(index) + ')');
			//on récupère l'index de l'image
			var _currentIndex = $target.attr('data-index');
			//on annule l'effet "selected"
			this.list_dots.find('button.selected').removeClass('selected');
			//on ajoute l'effet "selected"
			$target.addClass('selected');
			//Mise à jour de sa position
			this.listWrapper.animate({
				left : -parseInt(this._width)*_currentIndex+"px"
			}, this.duration);
		},
		//affiche l'image choisie
		showImage : function(e) {
			//alias de this
			var $target = $(e.target);
			
			//on récupère l'index de l'image
			this.currentIndex = $target.attr('data-index');
			
			//on annule l'effet "selected"
			this.list_dots.find('button.selected').removeClass('selected');
			//on ajoute l'effet "selected"
			$target.addClass('selected');
			
			//Mise à jour de sa position
			this.listWrapper.animate({
				left : -parseInt(this._width)*this.currentIndex+"px"
			}, this.duration);
			
			//on annule le timer si l'utilisateur le souhaite
			if (!this.focusReplay) {
				window.clearTimeout(this.focusTimeOut);
			}
		}
	});
	
	
	
	//Vue pour les galeries d'images avec pagination à flèches
	var carrouselArrowView = Backbone.View.extend({
		el : $('.carrousel-arrow'),
		listWrapper : null,
		_width : 0,
		currentIndex : 0,
		maxIndex : 0,
		duration : 500,
		//gestion des évènements
		events : {
			'click .arrow-next':'showNextImage',
			'click .arrow-back':'showPreviousImage',
			'keydown .arrow_mask':'keyboardSelection'
		},
		initialize : function () {
			//alias de this
			var _this = this;
			
			//on cache la 1ère flèche
			this.el.find('.arrow-back').css({
				visibility:"hidden"
			});
			
			//on défini le wrapper
			this.listWrapper = this.el.find(".image_arrow_wrapper");
			this.el.find('.arrow_mask').attr('tabindex', '0');
			
			//on défini la largeur de la boîte
			this._width = this.listWrapper.find('li:first-child').css('width');
			
			//Pour chaque puce d'image
			this.listWrapper.find('li').each(function (e){
				var $this = $(this);
				//si la classe selected est détectée
				if ($this.hasClass('selected')) {
					//on attribue l'élément au current index
					_this.currentIndex = e;
				}
				//Retire le focus du lien pour garder celui de frise
				$this.attr('tabindex', '-1');
				$this.find('a').attr('tabindex', '-1');
				
				//on récupère l'index maximum
				_this.maxIndex = e;
			});
			
			//on appel le rendu
			this.render();
		},
		//méthode du rendu
		render : function () {
		},
		//Récupération de l'image suivant l'index
		getIndexedImage : function (index) {
			return $(this.listWrapper.find('li')[index]);
		},
		//afficher l'image suivante
		showNextImage : function (e) {
			//Si l'index maximum est atteint
			if (this.currentIndex >= this.maxIndex) {
				//on retourne la méthode
				return;
			}
			
			this.el.find('.arrow-back').css({
				visibility:"visible"
			});
			
			//Récupération de l'image actuelle
			var $currentImage = this.getIndexedImage(this.currentIndex);
			//On enlève la sélection de l'image
			$currentImage.removeClass("selected");
			//on incrémente l'index de l'image
			this.currentIndex++;
			//Récupération de l'image suivante
			var $nextImage = this.getIndexedImage(this.currentIndex);
			//On ajoute la classe sélectionnée
			$nextImage.addClass('selected');
			
			//Mise à jour de sa position
			this.listWrapper.animate({
				left : -parseInt(this._width)*this.currentIndex+"px"
			}, this.duration);
			
			//Si l'index maximum est atteint
			if (this.currentIndex >= this.maxIndex) {
				this.el.find('.arrow-next').css({
					visibility:"hidden"
				});
			}
		},
		//afficher l'image suivante
		showPreviousImage : function (e) {
			//Si l'index minimum est atteint
			if (this.currentIndex <= 0) {
				//on retourne la méthode
				return;
			}
			
			this.el.find('.arrow-next').css({
				visibility:"visible"
			});
			
			//Récupération de l'image actuelle
			var $currentImage = this.getIndexedImage(this.currentIndex);
			//On enlève la sélection de l'image
			$currentImage.removeClass("selected");
			//on incrémente l'index de l'image
			this.currentIndex--;
			//Récupération de l'image suivante
			var $nextImage = this.getIndexedImage(this.currentIndex);
			//On ajoute la classe sélectionnée
			$nextImage.addClass('selected');
			
			//Mise à jour de sa position
			this.listWrapper.animate({
				left : -parseInt(this._width)*this.currentIndex+"px"
			}, this.duration);
			
			//Si l'index minimum est atteint
			if (this.currentIndex <= 0) {
				//on retourne la méthode
				this.el.find('.arrow-back').css({
					visibility:"hidden"
				});
			}
		},
		//navigation au clavier
		keyboardSelection : function (e) {
			switch (e.keyCode) {
				//fleche gauche
				case 37 :
					this.showPreviousImage();
				break;
				
				//fleche droite
				case 39 :
					this.showNextImage();
				break;
				
				//Par défaut : rien!
				default : break;
			}
		}
	});
	
	
	
	//Vue pour les galeries d'objects via Kewego
	var objectListView = Backbone.View.extend({
		el : $('.home_footer'),
		listWrapper : null,
		list_dots : null,
		object : 'object',
		_width : 0,
		currentIndex : 0,
		maxIndex : 0,
		duration : 500,
		//gestion des évènements
		events : {
			'click .object-link':'showObject'
		},
		initialize : function () {
			//alias de this
			var _this = this;
			
			//on défini le wrapper
			this.listWrapper = this.el.find(".objects");
			
			//on définit l'élément à rechercher : object ou div (pour IE7)
			this.object = ($(this.listWrapper.find('object')).length) ? 'object' : 'div';
			
			//on définit la hauteur du wrapper
			var _height =  $(this.listWrapper.find('' + this.object)[0]).css('height');
			this.listWrapper.css('height', _height);
			
			//Pour chaque element object / div
			this.listWrapper.find('' + this.object).each(function (e){
				var $this = $(this);
				//on cache les autres objects
				if (0 < e)
				{
					$this.css({
						display:"none"
					});
				}
				
				//Retire le focus du lien pour garder celui de frise
				$this.attr('tabindex', '-1');
			});
			
			//on appel le rendu
			this.render();
		},
		//méthode du rendu
		render : function () {
		},
		//affiche l'image choisie
		showObject : function(e) {
			//alias de this
			var $target = $(e.target);
			
			//on récupère l'attribut href
			var _href = $target.attr('href');
			_href = _href.substring(_href.indexOf('#'));
			
			//on cache tous les objects
			this.listWrapper.find('' + this.object).css({
				display:"none"
			});
			
			//si on est dans le cas IE7, on va chercher la classe CSS et non l'ID
			if ('div' == this.object)
			{
				_href = _href.replace('#', '.');
			}
			
			//on affiche celui qui nous intéresse
			this.listWrapper.find('' + _href).css({
				display:"block"
			});
			
			//on retourne la fonction
			e.preventDefault();
		}
	});
	
	
	
	//Méthode de gestion du site
	var mainView = Backbone.View.extend({
		//sélection de l'environnement
		el: $('body'),
		//rootage des évènements
		events: {
			//sur focus d'un champ
			'focus .searchForm input':'hideFieldLabel',
			//sur perte de focus d'un champ
			'blur .searchForm input':'showFieldLabel',
			//Au rollover d'un item du menu
			'focus a':'showSubmenu',
			'mouseenter a':'showSubmenu',
			//sur click hors de la zone du menu
			'click':'hideSubmenu',
			'mouseleave':'hideSubmenu',
			//Au click sur un lien d'évitement
			'click .next_theme':'showNextSubmenu',
			//Au rollover d'un lien du menu, désactiver le menu sélectionné
			'hover a':'removeSelectedSubmenu',
			//Ouverture de l'acces rapide
			'click .menu_quick_access':"openQuickAccess",
			'focus .quickLink':"focusQuickAccess",
			'blur .quickLink':"blurQuickAccess",
			//Zoom
			'click .zoomIn':'zoomIn',
			'click .zoomOut':'zoomOut',
			//Impression
			'click .print':'print',
			//ouverture du sous menu
			'click .menuLeftItem':'openSousMenuLeft',
			//ouverture du sous menu
			'focus .menuLeftItem':'openSousMenuLeft',
			//image au roll-over/out
			'mouseenter .addboximages':'showImageOnHover',
			'focus .addboxlink':'showImageOnFocus',
			'mouseleave .addboximages':'hideImageOnHover',
			'blur .addboxlink':'hideImageOnBlur',
			//changement d'option dans la selectbox itemsperpage
			'change .itemsperpage':'changeItemsPerPage',
			//clear all selected links
			'click .clear_all_link':'clearChecked',
			//changement du métier en fonction du domaine d'activité
			'change .blockdomain':'displayGoodWork',
			//changement de l'année de délibération
			'change .blockyear':'displayGoodValue',
			//clic sur les liens d'affichage / masquage des questions
			'click .faqactions .toggle':'toggleFaq',
			'focus .faqactions .toggle':'toggleFaq',
			//'click .faqactions .hide':'hideFaq',
			//'focus .faqactions .hide':'hideFaq',
			//clic sur une question pour afficher/masquer la réponse
			'click .faqactions .tx-irfaq-dynheader':'toggleAnswer',
			//clic sur les liens d'affichage / masquage du sitemap
			'click .sitemapactions .expAll .toggle':'toggleSitemap',
			//'click .sitemapactions .expAll .hide':'hideSitemap',
			//clic sur un dossier pour afficher/masquer son contenu
			'click .sitemapactions ol a':'toggleFolder',
			//click sur un item du magazine
			'click .menu_access .leftside a':'showItemMagazine',
			'click .menu_access .go-to-content a':'showItemMagazine',
			'click .menu_magazine .linked a':'showItemMagazine',
			'focus .menu_magazine .linked a':'showItemMagazine',
			//navigation aux flèches magazine
			'click .magazinenews .previous':'previousItemMagazine',
			'focus .magazinenews .previous':'previousItemMagazine',
			'click .magazinenews .next':'nextItemMagazine',
			'focus .magazinenews .next':'nextItemMagazine',
			//suppression du block tag
			'click .delete-tag':'deleteTags'
		},
		//initialisation
		initialize: function(){
			//Appel du rendu
			this.render();
			
			//si il y a un slide show
			if ($('.slide_show').length) {
				//On instancie la vue du slideshow
				var slideShow = new slideShowView;
			}
			//si il y a un slide show
			if ($('.home_news').length) {
				//On instancie la vue du slideshow
				var newsHome = new newsHomeView;
			}
			//si il y a un slide show
			if ($('.carrousel-arrow').length) {
				//On instancie la vue du slideshow
				var carrouselArrow = new carrouselArrowView;
			}
			//si il y a un slide show
			if ($('.carrousel-dots').length) {
				//On instancie la vue du slideshow
				var carrouselDot = new carrouselDotView;
			}
			//si il y a un slide show
			if ($('.objects_list').length) {
				//On instancie la vue du slideshow
				var objectList = new objectListView;
			}
			
			//PrettyPhoto
			$('.prettyModal').prettyPhoto({
				autoplay:false,
				show_title:true,
				overlay_gallery:false,
				deeplinking:false,
				social_tools:''
			});
			
			//TinyMCE
			$('.tinymce textarea').tinymce({
				script_url:'js/libs/tiny_mce/tiny_mce.js',
				theme:'advanced',
				entity_encoding:'raw',
				plugins: 'autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist',
				theme_advanced_buttons1: 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect',
				theme_advanced_buttons2: 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor',
				theme_advanced_buttons3:'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen',
				theme_advanced_buttons4: 'insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak',
				theme_advanced_toolbar_location:'top',
				theme_advanced_toolbar_align:'left',
				skin:'o2k7',
				skin_variant:'silver'
			});
			
			//calendar options
			$.tools.dateinput.localize('fr',{
				months:'janvier,f&eacute;vrier,mars,avril,mai,juin,juillet,ao&ucirc;t,septembre,octobre,novembre,d&eacute;cembre',
				shortMonths:'jan,f&eacute;v,mar,avr,mai,jun,jul,ao&ucirc;,sep,oct,nov,d&eacute;c',
				days:'dimanche,lundi,mardi,mercredi,jeudi,vendredi,samedi',
				shortDays:'dim,lun,mar,mer,jeu,ven,sam'
			});
			$('.calendar').dateinput({
				lang:'fr',
				format:'yyyy-mm-dd',
				firstDay:parseInt('0'),
				selectors:true,
				disabled:false,
				readonly:false,
				yearRange:[-99, 99]
			});
			
			//custom input file
			$inputFile = $('input:file');
			if ($inputFile.length) {
        $.each($inputFile, function(index, elem) {
        	$(this).customFileInput();
        });
			}
			
			//bloc metier
			$blockworks = $('.blockworks');
			if ($blockworks.length) {
				this.loadGoodWork($blockworks);
			}

			//bloc année
			$blockyear = $('.blockyear');
			if ($blockyear.length) {
				this.loadGoodValue($blockyear);
			}
		},
		//rendu
		render: function(){
		},
		//Masquage du label des champs
		hideFieldLabel: function(e){
			//selection du champ
			var input = $(e.target);
			//selection du label correspondant
			var label = $("label[for="+input.attr("id")+"]");
			//masquage du label
			label.fadeOut("slow");
		},
		//Affichage du label des champs
		showFieldLabel : function (e){
			//selection du champ
			var input = $(e.target);
			//selection du label correspondant
			var label = $("label[for="+input.attr("id")+"]");
			//si la valeur du champ est vide
			if (input.attr("value") === ""){
				//affichage du label
				label.fadeIn("slow");
			}
		},
		//Stockage de menu ouvert
		openedMenu : null,
		//Focus sur les menus
		showSubmenu : function (e) {
			var isMenuItem = false;
			//selection de l'element HTML
			var $target = $(e.target);
			//Pour chaque parent de la cible
			$target.parents().map(function () {
				//alias de this
				$this = $(this);
				//si le parent a la classe "menu-item"
				if ($this.hasClass('menu_item')) {
					isMenuItem = true;
					//alors on masque tous les menus
					$('.menu_item').removeClass('selected');
					//et on affiche le menu sélectionné
					$this.addClass('selected');
					//on assigne le drapeau du menu
					this.openedMenu = $this;
					//on retourne le fonction
					return;
				}
			});
			
			(!isMenuItem)?$(".menu_item").removeClass("selected"):void(0);
		},
		//Sur click out du menu
		hideSubmenu : function (e) {
			//Drapeau du menu
			var isMenuItem = false;
			var isQuickMenu = false;
			
			var $target = $(e.target);
			
			//Pour chaque parent de la cible
			$target.parents().map(function () {
				//alias de this
				$this = $(this);
				//si le parent a la classe "menu-item"
				if ($this.hasClass('menu_item')) {
					//alors on passe le drapeau à vrai
					isMenuItem = true;
					return;
				}
				
				//si le parent a la classe "menu-item"
				if ($this.hasClass('menu_quick_access')) {
					//alors on passe le drapeau à vrai
					isQuickMenu = true;
					return;
				}
			});
			//si on a clické sur le menu, on ne fait rien, sinon on ferme le menu
			(isMenuItem)?void(0):$(".menu_item").removeClass("selected");
			//fermeture du quick menu
			(isQuickMenu)?void(0):$(".menu_quick_access").removeClass('opened');
		},
		//showNextSubmenu
		showNextSubmenu : function (e) {
			e.preventDefault();
			//selection de l'element HTML
			var $target = $(e.target);
			//récupération de l'attribut HTML et du parent ".menu_item"
			var _href = $target.attr('href');
			var _sub = _href.indexOf('#');
			_href = _href.substring(_sub);
			var _closest = $('' + _href).parent('.menu_item');
			//alors on masque tous les menus
			$('.menu_item').removeClass('selected');
			//et on affiche le menu sélectionné
			_closest.addClass('selected');
			this.openedMenu = $(_closest);
			//on donne le focus au lien du menu choisi
			$('> a', _closest).focus();
			//on retourne la fonction
			return;
		},
		//removeSelectedSubmenu
		removeSelectedSubmenu : function(e) {
			var isMenuItem = false;
			//selection de l'element HTML
			var $target = $(e.target);
			//Pour chaque parent de la cible
			$target.parents().map(function () {
				//alias de this
				$this = $(this);
				//si le parent a la classe "menu-item"
				if ($this.hasClass('menu_item')) {
					isMenuItem = true;
					//alors on masque tous les menus
					$('.menu_item').removeClass('selected');
				}
			});
			
			(!isMenuItem)?$(".menu_item").removeClass("selected"):void(0);
		},
		//openQuickAccess
		openQuickAccess : function (e) {
			//alias
			var $quickAccessWrapper = $(".menu_quick_access");
			($quickAccessWrapper.hasClass("opened"))?$quickAccessWrapper.removeClass('opened'):$quickAccessWrapper.addClass('opened');
		},
		//focusQuickAccess
		focusQuickAccess : function (e) {
			//alias
			var $quickAccessWrapper = $(".menu_quick_access");
			$quickAccessWrapper.addClass('opened');
		},
		//blurQuickAccess
		blurQuickAccess : function (e) {
			//alias
			var $quickAccessWrapper = $(".menu_quick_access");
			$quickAccessWrapper.removeClass('opened');
		},
		//Gestion du zoom
		currentZoom : 1,
		//zoom in
		zoomIn : function (e) {
			e.preventDefault();
			this.currentZoom = this.currentZoom + 0.1;
			$('.page_content .article').css('font-size', this.currentZoom+"em");
			$('.toolBox').css('font-size', this.toolBoxZoom+"em");
		},
		//zoom out
		zoomOut : function (e) {
			e.preventDefault();
			this.currentZoom = this.currentZoom - 0.1;
			$('.page_content .article').css('font-size', this.currentZoom+"em");
		},
		//print
		print : function (e) {
			e.preventDefault();
			window.print();
		},
		//ouverture du sous-menu gauche
		openSousMenuLeft : function (e) {
			var $target = $(e.target).parent();
			$(".menuLeftItem").parent().removeClass('opened');
			$target.addClass('opened');
			return false;
		},
		//affichage image au roll-over
		showImageOnHover : function(e) {
			//on récupère toutes les infos de l'image survolée
			var $target = $(e.target);
			
			//on annule l'effet de survol
			this.hideImage();
			
			//on récupère la source de l'image à afficher au survol ainsi que son texte alternatif
			var _src = $target.attr('src');
			var _href = $target.parent('a').attr('rel');
			var _alt = $target.attr('alt');
			var _title = _alt + ' - ' + $target.parent('a').attr('title');
			
			//on modifie la source et le texte alternatif, et on ajoute les datas utiles
			$target.attr('src', _href);
			$target.attr('alt', _title);
			$target.attr('data-src', _src);
			$target.attr('data-alt', _alt);
		},
		//annule l'effet de survol
		hideImage : function() {
			//on annule le survol pour toutes les autres images
			this.el.find('img').each(function(e){
				var _this = this;
				
				//si l'image contient un attribut "data-src", cela signifi qu'elle contient encore l'effet de survol
				if ($(_this).attr('data-src') !== undefined)
				{
					//on réinitialise la source
					$(_this).attr({
						src : $(_this).attr('data-src'),
						alt : $(_this).attr('data-alt')
					});
					//on supprime l'attribut
					$(_this).removeAttr('data-src');
					$(_this).removeAttr('data-alt');
				}
			});
		},
		//affichage image au roll-over
		hideImageOnHover : function(e) {
			//on récupère toutes les infos de l'image créée
			var $target = $(e.target);
			//on réinitialise la source
			$target.attr({
				src : $target.attr('data-src'),
				alt : $target.attr('data-alt')
			});
			//on supprime l'attribut
			$target.removeAttr('data-src');
			$target.removeAttr('data-alt');
		},
		//affichage image au roll-over
		showImageOnFocus : function(e) {
			//on récupère toutes les infos de l'image survolée
			var $target = $(e.target);
			
			//on annule l'effet de survol
			this.hideImage();
			
			//on récupère la source de l'image à afficher au survol ainsi que son texte alternatif
			var _src = $target.find('img').attr('src');
			var _href = $target.attr('rel');
			var _alt = $target.find('img').attr('alt');
			var _title = _alt + ' - ' + $target.attr('title');
			
			//on modifie la source et le texte alternatif, et on ajoute les datas utiles
			$target.find('img').attr('src', _href);
			$target.find('img').attr('alt', _title);
			$target.find('img').attr('data-src', _src);
			$target.find('img').attr('data-alt', _alt);
		},
		//affichage image au roll-over
		hideImageOnBlur : function(e) {
			//on récupère toutes les infos de l'image créée
			var $target = $(e.target).find('img');
			//on réinitialise la source
			$target.attr({
				src : $target.attr('data-src'),
				alt : $target.attr('data-alt')
			});
			//on supprime l'attribut
			$target.removeAttr('data-src');
			$target.removeAttr('data-alt');
		},
		//changement d'option selectbox 
		changeItemsPerPage : function(e) {
			var input = $(e.target);
			var selected = input.find('option:selected');
			window.location = selected.val();
		},
		//clear all selected links
		clearChecked : function(e) {
			e.preventDefault();
			var $target = $(e.target);
			var $closest = $target.closest('.tx_powermail_pi1_fieldwrap_html');
			var inputsComponent = $closest.find('input:checkbox');
			inputsComponent.attr('checked', false);

			if ($closest.hasClass('blockworks'))
			{
				var rel = $closest.attr('rel');
				var inputsComponentRel = $('div.' + rel).find('input:checkbox');
				inputsComponentRel.attr('checked', false);

				$('div.' + rel).find('.active').addClass('inactive').removeClass('active');
				$('#default').removeClass('inactive').addClass('active');
			}
		},
		//changement du métier en fonction du domaine d'activité
		loadGoodWork : function(container) {
			var relation = container.attr('rel');
			var $checked = container.find('input:checked');
			var _ok = 0;
			
			$('div.' + relation).find('.active').addClass('inactive').removeClass('active');
			
			if ($checked.length > 0)
			{
				$('#nowork').removeClass('active').addClass('inactive');
				$('#default').removeClass('active').addClass('inactive');
				
				$.each($checked, function(index, element) {
					var _id = $(element).attr('id') + '_list';
					
					if ($('#' + _id).length > 0)
					{
						$('#' + _id).removeClass('inactive').addClass('active');
						_ok++;
					}
				});
				
				if (_ok == 0)
				{
					$('#nowork').removeClass('inactive').addClass('active');
					$('#default').removeClass('active').addClass('inactive');
				}
			}
			else
			{
				$('#nowork').removeClass('active').addClass('inactive');
				$('#default').removeClass('inactive').addClass('active');
			}
		},
		displayGoodWork : function(e) {
			var input = $(e.target);
			var container = input.closest('.tx_powermail_pi1_fieldwrap_html');
			var relation = $(container).attr('rel');
			var $checked = $(container).find('input:checked');
			var _ok = 0;
			
			$('div.' + relation).find('.active').addClass('inactive').removeClass('active');
			
			if ($checked.length > 0)
			{
				$('#nowork').removeClass('active').addClass('inactive');
				$('#default').removeClass('active').addClass('inactive');
				
				$.each($checked, function(index, element) {
					var _id = $(element).attr('id') + '_list';
					
					if ($('#' + _id).length > 0)
					{
						$('#' + _id).removeClass('inactive').addClass('active');
						_ok++;
					}
				});
				
				if (_ok == 0)
				{
					$('#nowork').removeClass('inactive').addClass('active');
					$('#default').removeClass('active').addClass('inactive');
				}
			}
			else
			{
				$('#nowork').removeClass('active').addClass('inactive');
				$('#default').removeClass('inactive').addClass('active');
			}
			
			if (!input.is(':checked'))
			{
				var _id = input.attr('id') + '_list';
				$('#' + _id).find('input:checkbox').attr('checked', false);
			}
		},
		//changement de la séance en fonction de l'année
		loadGoodValue: function(container) {
			container.change();
		},
		displayGoodValue : function(e) {
			var input = $(e.target);
			var selected = input.find('option:selected').val();
			var container = input.closest('.tx_powermail_pi1_fieldwrap_html');
			var relation = $(container).attr('rel');

			$('#' + relation).find('option:selected').removeAttr('selected');
			$('#' + relation).find('option.all').attr('selected', true);

			if ('all' == selected)
			{
				$('#' + relation + ' option').show();
			}
			else
			{
				$('#' + relation + ' option').hide();
				$('#' + relation + ' option.' + selected).show();
				$('#' + relation + ' option.all').show();
			}
		},
		//clic sur les liens d'affichage / masquage des questions
		toggleFaq : function(e) {
			e.preventDefault();
			var link = $(e.target);
			var faq = link.closest('.faqactions');

			if (faq.hasClass('faq-displayed')) //fermeture
			{
				faq.find('dd').removeClass('tx-irfaq-dynans-display').addClass('tx-irfaq-dynans-hidden');
				faq.find('.sign-minus').hide();
				faq.find('.sign-plus').show();
				faq.removeClass('faq-displayed').addClass('faq-hidden');
				link.parent('p').removeClass('off');
			}
			else //ouverture
			{
				faq.find('dd').removeClass('tx-irfaq-dynans-hidden').addClass('tx-irfaq-dynans-display');
				faq.find('.sign-minus').show();
				faq.find('.sign-plus').hide();
				faq.removeClass('faq-hidden').addClass('faq-displayed');
				link.parent('p').addClass('off');
			}

		},
		//clic sur une question pour afficher/masquer la réponse
		toggleAnswer : function(e) {
			e.preventDefault();
			var link = $(e.target);
			var dd = link.parent('.tx-irfaq-dynheader');
			var rel = dd.attr('rel');
			var target = $('#' + rel);

			if (target.hasClass('tx-irfaq-dynans-hidden'))
			{
				target.removeClass('tx-irfaq-dynans-hidden').addClass('tx-irfaq-dynans-display');
				dd.find('.sign-minus').show();
				dd.find('.sign-plus').hide();
			}
			else
			{
				target.removeClass('tx-irfaq-dynans-display').addClass('tx-irfaq-dynans-hidden');
				dd.find('.sign-minus').hide();
				dd.find('.sign-plus').show();
			}
		},
		//clic sur les liens d'affichage / masquage du sitemap
		toggleSitemap : function(e) {
			e.preventDefault();
			var link = $(e.target);
			var site = link.closest('.sitemapactions').children('ol');

			if (site.hasClass('sitemap-displayed')) //fermeture
			{
				site.find('ol').removeClass('ol-display').addClass('ol-hidden');
				site.find('img.on').show();
				site.find('img.off').hide();
				site.removeClass('sitemap-displayed').addClass('sitemap-hidden');
				link.parent('.expAll').removeClass('off');
			}
			else //ouverture
			{
				site.find('ol').removeClass('ol-hidden').addClass('ol-display');
				site.find('img.on').hide();
				site.find('img.off').show();
				site.removeClass('sitemap-hidden').addClass('sitemap-displayed');
				link.parent('.expAll').addClass('off');
			}
		},
		/*hideSitemap : function(e) {
			e.preventDefault();
			var link = $(e.target);
			var site = link.closest('.sitemapactions').children('ol');
			site.find('ol').removeClass('ol-display').addClass('ol-hidden');
			site.find('img.on').show();
			site.find('img.off').hide();
		},*/
		//clic sur un dossier pour afficher/masquer son contenu
		toggleFolder : function(e) {
			var link = $(e.target);
			if (link.closest('span').hasClass('ifsub'))
			{
				e.preventDefault();
				var target = link.closest('div');
				var children = target.children('ol');
				var imgon = target.children('img.on');
				var imgoff = target.children('img.off');
				if (children.hasClass('ol-display'))
				{
					children.removeClass('ol-display').addClass('ol-hidden');
					imgon.show();
					imgoff.hide();
				}
				else if (children.hasClass('ol-hidden'))
				{
					children.removeClass('ol-hidden').addClass('ol-display');
					imgon.hide();
					imgoff.show();
				}
				else
				{
					children.addClass('ol-display');
					imgon.hide();
					imgoff.show();
				}
			}
		},
		//click sur un item du magazine
		showItemMagazine : function(e) {
			e.preventDefault();
			var link = $(e.target);
			var rel = link.attr('rel');

			$('.menu_magazine').find('a').removeClass('active');
			link.addClass('active');
			$('.magazinenews').hide();
			
			if($('#' + rel).length && "lien0" == rel)
			{
				$('.menu_access .leftside a').hide();
				$('#' + rel).show();
				$('.magazinecontent').removeClass('magazinecontent').addClass('magazineaccueil');
			}
			else if ($('#' + rel).length)
			{
				$('.menu_access .leftside a').show();
				$('#' + rel).show();
				$('.magazineaccueil').removeClass('magazineaccueil').addClass('magazinecontent');
			}
			else
			{
				$('.menu_access .leftside a').show();
				$('.magazinecontent').removeClass('magazinecontent').addClass('magazineaccueil');
			}
			
			$('.magazinenews:first-child').find('a.previous').hide();
			$('.magazinenews:first-child').find('a.next').hide();
			$('.magazinenews:nth-child(2)').find('a.previous').hide();
			$('.magazinenews:last-child').find('a.next').hide();
		},
		//navigation aux flèches magazine
		previousItemMagazine : function(e) {
			e.preventDefault();
			var current = $(e.target).closest('.magazinenews');
			var previous = current.prev();
			var rel = previous.attr('id');
			current.hide();
			previous.show();
			$('.menu_magazine a').removeClass('active');
			$('.menu_magazine a[rel^="' + rel + '"]').addClass('active');
		},
		nextItemMagazine : function(e) {
			e.preventDefault();
			var current = $(e.target).closest('.magazinenews');
			var next = current.next();
			var rel = next.attr('id');
			current.hide();
			next.show();
			$('.menu_magazine a').removeClass('active');
			$('.menu_magazine a[rel^="' + rel + '"]').addClass('active');
		},
		//suppression du bloc tags
		deleteTags : function(e) {
			e.preventDefault();
			var link = $(e.target);
			var parent = link.closest('div.tag');
			parent.prev().remove();
			parent.remove();
		}
	});
	
	return new mainView;
});
