define([
	'order!libs/jquery/jquery-tools',
	'order!libs/prettyPhoto/prettyPhoto',
	'order!libs/tiny_mce/jquery-tinymce',
	'order!libs/fileinput/jquery-fileinput',
	'order!libs/underscore/underscore-min',
	'order!libs/backbone/backbone-min'
],
function(){
	return {
		Backbone: Backbone.noConflict(),
		_: _.noConflict(),
		$: jQuery.noConflict()
	};
});