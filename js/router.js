// Filename: router.js
define([
	//Sélection des librairies
	'jQuery',
	'Underscore',
	'Backbone',
	
	//Sélection des vues pour chaque module
	//selection par défaut
	'views/main/main'
], function($, _, Backbone, mainView, realisationsView, presentationView, laboratoryView, contactsView, linksView, networkView){
	var AppRouter = Backbone.Router.extend({
		routes: {
			//--- Liaison des modules suivant les urls
			//--- Actions par défaut sur le site
			'*actions': 'defaultAction'
		},
		//Initialisation de la vue par défaut
		defaultAction: function(actions){
			// We have no matching route, lets display the home page 
			mainView.render(); 
		}
	});

	//initialisation du routeur
	var initialize = function(){
		//insticiation du routeur
		var app_router = new AppRouter;
		//initialisation de l'historique
		Backbone.history.start();
	};
	//retourne l'initialisation
	return { 
		initialize: initialize
	};
});